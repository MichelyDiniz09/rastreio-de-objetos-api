Instruções !!

Primeiramente, o postgresql deve ser previamente instalado e iniciado no sistema operacional.

Para iniciar projeto deve ser importado no eclipse o diretório onde foi clonado 
o projeto. Em seguida, a IDE vai configurar o projeto e no arquivo application.properties
que fica no diretório src/main/resources deve ser alteradas as configurações de acesso a database local.

Em seguida, o package com.teste.transportadora há um arquivo chamado TransportadoraApplication.java, 
é só abrir esse arquivo e dar start na aplicação utilizando Java Aplication. Feito isso a aplicação
vai criar a tabela necessária.

No diretório src/main/resources também há um arquivo data.sql, caso seja necessário carregar a tabela manualmente.