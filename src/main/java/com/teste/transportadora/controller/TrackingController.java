package com.teste.transportadora.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teste.transportadora.controller.dto.DeliveryDto;
import com.teste.transportadora.model.Delivery;
import com.teste.transportadora.repository.DeliveryRepository;

@RestController
public class TrackingController {

	@Autowired
	private DeliveryRepository deliveryRepository;
	
	@RequestMapping("/object")
	public List<DeliveryDto> object(String track) {
		if (track == null) {
			List<Delivery> deliveries = deliveryRepository.findAll();
			return DeliveryDto.convert(deliveries);
		} else {
			List<Delivery> deliveries = deliveryRepository.findByTrack(track);
			return DeliveryDto.convert(deliveries);	
		}		
		
	}	
	
}
