package com.teste.transportadora.controller.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.teste.transportadora.model.Delivery;

public class DeliveryDto {
	
	private Long id;
	private String track;
	private String status;
	private LocalDateTime dateUpdate;
	
	public DeliveryDto (Delivery delivery) {
		this.id = delivery.getId();
		this.track = delivery.getTrack();
		this.status = delivery.getStatus();
		this.dateUpdate = delivery.getDateUpdate();				
	}
	
	public Long getId() {
		return id;
	}
	
	public String getTrack() {
		return track;
	}
	
	public String getStatus() {
		return status;
	}

	public LocalDateTime getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(LocalDateTime dateUpdate) {
		this.dateUpdate = dateUpdate;
	}
	
	public static List<DeliveryDto> convert(List<Delivery> delivery) {
		return delivery.stream().map(DeliveryDto::new).collect(Collectors.toList());
		
	}


}
