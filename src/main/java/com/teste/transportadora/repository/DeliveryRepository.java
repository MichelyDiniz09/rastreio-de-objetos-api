package com.teste.transportadora.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teste.transportadora.model.Delivery;

public interface DeliveryRepository extends JpaRepository<Delivery, Long>{

	List<Delivery> findByTrack(String track);

}
