package com.teste.transportadora.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_deliveries")
public class Delivery implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	 
	private String track;
	
	private String status; 
	
	private LocalDateTime dateUpdate = LocalDateTime.now();

	
	public Long getId() {
		return id;
	}
	
	public String getTrack() {
		return track;
	}
	
	public String getStatus() {
		return status;
	}

	public LocalDateTime getDateUpdate() {
		return dateUpdate;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
 
	public void setTrack(String track) {
		this.track = track;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setDateUpdate(LocalDateTime dateUpdate) {
		this.dateUpdate = dateUpdate;
	}
	
}
